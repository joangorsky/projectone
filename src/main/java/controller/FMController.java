package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Response;
import services.FMService;
import services.FMServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class FMController {

    private static FMController fmController;
    FMService fmService;
    private FMController(){
        fmService = new FMServiceImpl();
    }

    public static FMController getInstance(){
        if(fmController == null)
            fmController = new FMController();

        return fmController;
    }

    public void viewAllReimbursements(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        out.println(new ObjectMapper().writeValueAsString(new Response("All Reimbursements Tickets Retrieved", true, fmService.viewAllReimbursements())));

    }

    public void approveOrDenyReq(HttpServletRequest req, HttpServletResponse resp) throws IOException {
      //  System.out.println("inFM controller");
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer reimbid = Integer.parseInt(req.getParameter("reimbid"));
        Integer reimb_status_id_fk = Integer.parseInt(req.getParameter("reimb_status_id_fk"));

        fmService.approveOrDenyReq(reimbid, reimb_status_id_fk);

        out.println(new ObjectMapper().writeValueAsString(new Response("Approve or Deny Request Performed", true, null)));

    }

    public void viewAllPendingReq(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer reimb_status_id_fk = Integer.parseInt(req.getParameter("reimb_status_id_fk"));


        out.println(new ObjectMapper().writeValueAsString(new Response("All Pending Requests Retrieved", true, fmService.viewAllPendingReq(reimb_status_id_fk))));

    }


}
