package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Reimbursement;
import models.Response;
import services.FMService;
import services.FMServiceImpl;
import services.ReimbursementService;
import services.ReimbursementServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class ReimbursementController {
    private static ReimbursementController reimbursementController;
    ReimbursementService reimbursementService;
    FMService fmService;

    private ReimbursementController() {

        reimbursementService = new ReimbursementServiceImpl();
        fmService = new FMServiceImpl();
    }

    public static ReimbursementController getInstance() {
        if (reimbursementController == null)
            reimbursementController = new ReimbursementController();

        return reimbursementController;
    }

    public void getAllPastTickets(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("application/json");

        PrintWriter out = resp.getWriter();

        Integer userid = Integer.parseInt(req.getParameter("userid"));

        out.println(new ObjectMapper().writeValueAsString(new Response("Past Tickets Retreived", true, reimbursementService.getAllPastTickets(userid))));
    }

    public void addReimbReq(HttpServletRequest req, HttpServletResponse resp) throws IOException {
       // System.out.println("in controller - addreimbreq");
        resp.setContentType("application/json");

        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        Reimbursement reimbursement = new ObjectMapper().readValue(requestBody, Reimbursement.class);

        reimbursementService.addReimbReq(reimbursement);

        out.println(new ObjectMapper().writeValueAsString(new Response("Added a New Reimbursement Request", true, null)));
    }

    public void viewStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("application/json");

        PrintWriter out = resp.getWriter();


        Integer userid = Integer.parseInt(req.getParameter("userid"));


        out.println(new ObjectMapper().writeValueAsString(new Response("Status of the Request Retrieved", true, reimbursementService.viewStatus(userid))));
    }

}
