package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Response;
import models.User;
import services.UserService;
import services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class UserController {

    private static UserController userController;
    UserService userService;

    private UserController()
    {
        userService = new UserServiceImpl();
    }

    public static UserController getInstance(){
        if(userController == null)
            userController = new UserController();

        return userController;
    }

    public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        //  System.out.println(" in login method");

          resp.setContentType("application/json");

          PrintWriter out = resp.getWriter();

          String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

          User user = new ObjectMapper().readValue(requestBody,User.class);

          User tempUser = userService.login(user);

          if(tempUser != null){

            //create session if successful
            HttpSession httpSession = req.getSession(true);
            httpSession.setAttribute("userObj", tempUser);

            out.println(new ObjectMapper().writeValueAsString(new Response("login successful", true, tempUser)));
             }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("invalid username or password", false, null)));
              }

    }

    public void register(HttpServletRequest req, HttpServletResponse resp) throws IOException {

      //  System.out.println("in register method");
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        User user = new ObjectMapper().readValue(requestBody,User.class);

        //try to create user
        User tempUser = userService.register(user);

        if(tempUser != null){
            //if user was created
            out.println(new ObjectMapper().writeValueAsString(new Response("user has been created", true, tempUser)));
        }else{
            //if username already exists in the system
            out.println(new ObjectMapper().writeValueAsString(new Response("username already exists in the system", false, null)));

        }
    }

    public void checkSession(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        User user = (User) req.getSession().getAttribute("userObj");

        if(user != null){
            out.println(new ObjectMapper().writeValueAsString(new Response("session found", true, user)));
        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("no session womp womp", false, null)));
        }
    }

    public void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        req.getSession().setAttribute("userObj", null);

        out.println(new ObjectMapper().writeValueAsString(new Response("session terminated",true,null)));
    }



}
