package dao;

import models.Reimbursement;

import java.util.List;

public interface FMDao {

    List<Reimbursement> viewAllReimbursements();
    void approveOrDenyReq(Integer reimb_id, Integer reimb_status_id_fk);
    List<Reimbursement> viewAllPendingReq(Integer reimb_status_id_fk);
}
