package dao;

import models.Reimbursement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FMDaoImpl implements FMDao {

    String url = "jdbc:postgresql://revaturedatabase.ctb3kqepskse.us-east-2.rds.amazonaws.com/ProjectOne_Expense_Reimbursement_System_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    private static FMDao fmDao;

    private FMDaoImpl() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static FMDao getInstance() {
        if (fmDao == null) {
            fmDao = new FMDaoImpl();
        }

        return fmDao;
    }


    @Override
    public List<Reimbursement> viewAllReimbursements() {
       // System.out.println("in the method");
        List<Reimbursement> reimbursement = new ArrayList<>();

        try {
            //generating our connection
            Connection conn = DriverManager.getConnection(url, username, password);

            //sql statement that we will be executing
            String sql = "SELECT * FROM ERS_REIMBURSEMENT;";
            PreparedStatement ps = conn.prepareStatement(sql);
            //execute the sql statement and return the result set
            ResultSet rs = ps.executeQuery();
            //loop through all records in the result set
            while (rs.next()) {
                reimbursement.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
                        rs.getTimestamp(4), rs.getString(5), rs.getByte(6), rs.getInt(7),
                        rs.getInt(8), rs.getInt(9), rs.getInt(10)));

            }
            //close the connection
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //return all data in the collection
        return reimbursement;
    }

    @Override
    public void approveOrDenyReq(Integer reimbid, Integer reimb_status_id_fk) {
       // System.out.println(" FMDao in approve or deny Req method");
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            //sql statement
            String sql = "update ERS_REIMBURSEMENT set Reimb_resolved = current_timestamp, Reimb_status_id_fk = ? where Reimb_id = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);

            //set question mark variables
            ps.setInt(1, reimb_status_id_fk);

            ps.setInt(2, reimbid);


            //execute
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Reimbursement> viewAllPendingReq(Integer reimb_status_id_fk) {
      //  System.out.println("in the view all pending method");
        List<Reimbursement> reimbursement = new ArrayList<>();

        try {
            //generating our connection
            Connection conn = DriverManager.getConnection(url, username, password);

            //sql statement that we will be executing
            String sql = "SELECT * FROM ERS_REIMBURSEMENT where Reimb_status_id_fk = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, reimb_status_id_fk);
            //execute the sql statement and return the result set
            ResultSet rs = ps.executeQuery();
            //loop through all records in the result set
            while (rs.next()) {
                reimbursement.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
                        rs.getTimestamp(4), rs.getString(5), rs.getByte(6), rs.getInt(7),
                        rs.getInt(8), rs.getInt(9), rs.getInt(10)));
                System.out.println(reimbursement);

            }

            //close the connection
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reimbursement;

    }
}