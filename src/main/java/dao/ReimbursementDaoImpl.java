package dao;

import models.Reimbursement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao{

    String url = "jdbc:postgresql://revaturedatabase.ctb3kqepskse.us-east-2.rds.amazonaws.com/ProjectOne_Expense_Reimbursement_System_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    private static ReimbursementDao reimbursementDao;

    private ReimbursementDaoImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static ReimbursementDao getInstance(){
        if(reimbursementDao == null)
            reimbursementDao = new ReimbursementDaoImpl();

        return reimbursementDao;
    }

    @Override
    public List<Reimbursement> getAllPastTickets(Integer userid) {
      //  System.out.println("in the get all past tickets - emp method");
        List<Reimbursement> reimbursement= new ArrayList<>();

        try{
            //generating our connection
            Connection conn = DriverManager.getConnection(url,username,password);

            //sql statement that we will be executing
            String sql = "SELECT * FROM ERS_REIMBURSEMENT where Reimb_author_fk = ? and Reimb_status_id_fk = 2 or Reimb_status_id_fk = 3;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, userid);
            //execute the sql statement and return the result set
            ResultSet rs = ps.executeQuery();
            //loop through all records in the result set
            while(rs.next()){
                reimbursement.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
                        rs.getTimestamp(4), rs.getString(5), rs.getByte(6), rs.getInt(7),
                        rs.getInt(8), rs.getInt(9), rs.getInt(10)));
                            }
            //close the connection
            conn.close();

        }catch(SQLException e){
            e.printStackTrace();
        }

        //return all data in the collection
        return reimbursement;
    }

    @Override
    public void addReimbReq(Reimbursement reimbursement) {
      //  System.out.println("in emp addreimb request method");
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            //sql statement
            String sql = "INSERT INTO ERS_REIMBURSEMENT VALUES (DEFAULT, ?, DEFAULT, null, ?, null, ?, 2, 1, ?);";
            PreparedStatement ps = conn.prepareStatement(sql);

            //set question mark variables
            ps.setInt(1, reimbursement.getReimb_amount());
           // ps.setTimestamp(2, reimbursement.getReimb_submitted());
           // ps.setTimestamp(2, reimbursement.getReimb_resolved());
            ps.setString(2, reimbursement.getReimb_description());
         //   ps.setByte(5, reimbursement.getReimb_receipt());
            ps.setInt(3, reimbursement.getReimb_author_fk());
          //  ps.setInt(5, reimbursement.getReimb_resolver_fk());
         //   ps.setInt(6, reimbursement.getReimb_status_id_fk());
            ps.setInt(4, reimbursement.getReimb_type_id_fk());

            //execute
            ps.executeUpdate();

        }catch(SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Reimbursement> viewStatus(Integer userid) {
       // System.out.println("in emp view status method");
        List<Reimbursement> reimbursement = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            //sql statement
            String sql = "Select * from ers_reimbursement where Reimb_author_fk = ? and Reimb_status_id_fk = 1;";
            PreparedStatement ps = conn.prepareStatement(sql);

            //set question mark variables
            ps.setInt(1, userid);

            ResultSet rs = ps.executeQuery();

            //loop through all records in the result set
            while(rs.next()) {
                reimbursement.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
                        rs.getTimestamp(4), rs.getString(5), rs.getByte(6), rs.getInt(7),
                        rs.getInt(8), rs.getInt(9), rs.getInt(10)));


            }

            //conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //return all data in the collection
        return reimbursement;
    }
    }

