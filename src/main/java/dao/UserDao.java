package dao;

import models.User;

public interface UserDao {

    void insertUser(User user);
    User getOneUser(String ers_username);
}
