package dao;

import models.User;

import java.sql.*;

public class UserDaoImpl implements UserDao{

    String url = "jdbc:postgresql://revaturedatabase.ctb3kqepskse.us-east-2.rds.amazonaws.com/ProjectOne_Expense_Reimbursement_System_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    private static UserDao userDao;

    public UserDaoImpl() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static UserDao getInstance() {
        if (userDao == null) {
            userDao = new UserDaoImpl();
        }

        return userDao;

    }


    @Override
    public void insertUser(User user) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            //sql statement
            String sql = "INSERT INTO ERS_USERS VALUES (DEFAULT, ?, ?, ?, ?, ?, ?);";
            PreparedStatement ps = conn.prepareStatement(sql);

            //set question mark variables
            ps.setString(1, user.getErs_username());
            ps.setString(2, user.getErs_password());
            ps.setString(3, user.getUser_first_name());
            ps.setString(4, user.getUser_last_name());
            ps.setString(5, user.getUser_email());
            ps.setInt(6, user.getUser_role_id_fk());


            //execute
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

            }

    @Override
    public User getOneUser(String ers_username) {
        User user = null;
       // System.out.println("in UserDaoimpl method");

        try (Connection conn = DriverManager.getConnection(url, username,  password)) {
            //sql statement

            String sql = "SELECT * FROM ERS_USERS WHERE Ers_username = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, ers_username);


            ResultSet rs = ps.executeQuery();
        //    System.out.println("inside try block");
            //this is iterating through the records
            while(rs.next()) {
                user = new User(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getInt(7));


               // System.out.println(user);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

}

