package frontcontroller;

import controller.FMController;
import controller.ReimbursementController;
import controller.UserController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//endpoint routing
@WebServlet(name="dispatcher", urlPatterns = "/api/*")
public class Dispatcher extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String URI = req.getRequestURI();
        System.out.println(URI);

        switch (URI) {
            case "/api/login":
                if (req.getMethod().equals("POST"))
                   // System.out.println("in dispatcher");
                    UserController.getInstance().login(req, resp);
                break;

            case "/api/fm/viewAllReimbursements":

                if(req.getMethod().equals("GET"))
                   // System.out.println("in fm viewallreimbursements");

                FMController.getInstance().viewAllReimbursements(req, resp);
                break;

            case "/api/fm/viewallpendingreq":
                if(req.getMethod().equals("GET"))
                  //  System.out.println(" in fm view all pending requests");
                    FMController.getInstance().viewAllPendingReq(req, resp);
                break;

            case "/api/fm/approveordenyreq":
                if(req.getMethod().equals("PUT"))
                  //  System.out.println("in dispatcher");
                    FMController.getInstance().approveOrDenyReq(req, resp);
                break;

            case "/api/emp/getallpasttickets":
                if(req.getMethod().equals("GET"))
                    ReimbursementController.getInstance().getAllPastTickets(req, resp);
                break;
            case "/api/emp/viewstatus":
                if(req.getMethod().equals("GET"))
                    ReimbursementController.getInstance().viewStatus(req, resp);
                break;
            case "/api/emp/addreimbreq":
                   if(req.getMethod().equals("POST"))
                   //    System.out.println("inDispatcher addreimbreq");
                    ReimbursementController.getInstance().addReimbReq(req, resp);
                break;
            case "/api/check-session":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().checkSession(req, resp);
                break;
            case "/api/logout":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().logout(req, resp);
                break;


        }
        }

        }









/*
 switch(URI){
            case "/ProjectOne/api/login":

                if(req.getMethod().equals("POST"))

                UserController.getInstance().login(req, resp);
                break;
           case "/api/register":
                if(req.getMethod().equals("POST"))
                    UserController.getInstance().register(req, resp);
                break;
            case "/api/emp/getallpasttickets":
                    if(req.getMethod().equals("GET"))
                     ReimbursementController.getInstance().getAllPastTickets(req, resp);
                    break;
            case "/ProjectOne/api/emp/addreimbreq":
                    if(req.getMethod().equals("POST"))
                    ReimbursementController.getInstance().addReimbReq(req, resp);
                    break;
           case "/api/emp/viewstatus":
                    if(req.getMethod().equals("GET"))
                    ReimbursementController.getInstance().viewStatus(req, resp);
                    break;
          case "/api/fm/viewallreimbursements":
                    if(req.getMethod().equals("GET"))
                    FMController.getInstance().viewAllReimbursements(req, resp);
                    break;

          case "/ProjectOne/api/fm/approveordenyreq":
                    if(req.getMethod().equals("PUT"))
                    FMController.getInstance().approveOrDenyReq(req, resp);
                    break;

           case "/ProjectOne/api/fm/viewallpendingreq":
                    if(req.getMethod().equals("GET"))
                    FMController.getInstance().viewAllPendingReq(req, resp);
                    break;
                    }
 */
