package models;

import java.sql.*;
import java.sql.Timestamp;

public class Reimbursement {

    private Integer reimb_id;
    private Integer reimb_amount;
    private Timestamp reimb_submitted;
    private Timestamp reimb_resolved;
    private String reimb_description;
    private Byte reimb_receipt;
    private Integer reimb_author_fk;
    private Integer reimb_resolver_fk;
    private Integer reimb_status_id_fk;
    private Integer reimb_type_id_fk;

    public Reimbursement() {
            }

    public Reimbursement(Integer reimb_id, Integer reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved, String reimb_description, Byte reimb_receipt, Integer reimb_author_fk, Integer reimb_resolver_fk, Integer reimb_status_id_fk, Integer reimb_type_id_fk) {
        this.reimb_id = reimb_id;
        this.reimb_amount = reimb_amount;
        this.reimb_submitted = reimb_submitted;
        this.reimb_resolved = reimb_resolved;
        this.reimb_description = reimb_description;
        this.reimb_receipt = reimb_receipt;
        this.reimb_author_fk = reimb_author_fk;
        this.reimb_resolver_fk = reimb_resolver_fk;
        this.reimb_status_id_fk = reimb_status_id_fk;
        this.reimb_type_id_fk = reimb_type_id_fk;
    }

    public Reimbursement(Integer reimb_id, Integer reimb_amount, Timestamp reimb_submitted,
                         Timestamp reimb_resolved, String reimb_description) {

        this.reimb_id = reimb_id;
        this.reimb_amount = reimb_amount;
        this.reimb_submitted = reimb_submitted;
        this.reimb_resolved = reimb_resolved;
        this.reimb_description = reimb_description;


    }


    public Integer getReimb_id() {
        return reimb_id;
    }

    public void setReimb_id(Integer reimb_id) {
        this.reimb_id = reimb_id;
    }

    public Integer getReimb_amount() {
        return reimb_amount;
    }

    public void setReimb_amount(Integer reimb_amount) {
        this.reimb_amount = reimb_amount;
    }

    public Timestamp getReimb_submitted() {
        return reimb_submitted;
    }

    public void setReimb_submitted(Timestamp reimb_submitted) {
        this.reimb_submitted = reimb_submitted;
    }

    public Timestamp getReimb_resolved() {
        return reimb_resolved;
    }

    public void setReimb_resolved(Timestamp reimb_resolved) {
        this.reimb_resolved = reimb_resolved;
    }

    public String getReimb_description() {
        return reimb_description;
    }

    public void setReimb_description(String reimb_description) {
        this.reimb_description = reimb_description;
    }

    public Byte getReimb_receipt() {
        return reimb_receipt;
    }

    public void setReimb_receipt(Byte reimb_receipt) {
        this.reimb_receipt = reimb_receipt;
    }

    public Integer getReimb_author_fk() {
        return reimb_author_fk;
    }

    public void setReimb_author_fk(Integer reimb_author_fk) {
        this.reimb_author_fk = reimb_author_fk;
    }

    public Integer getReimb_resolver_fk() {
        return reimb_resolver_fk;
    }

    public void setReimb_resolver_fk(Integer reimb_resolver_fk) {
        this.reimb_resolver_fk = reimb_resolver_fk;
    }

    public Integer getReimb_status_id_fk() {
        return reimb_status_id_fk;
    }

    public void setReimb_status_id_fk(Integer reimb_status_id_fk) {
        this.reimb_status_id_fk = reimb_status_id_fk;
    }

    public Integer getReimb_type_id_fk() {
        return reimb_type_id_fk;
    }

    public void setReimb_type_id_fk(Integer reimb_type_id_fk) {
        this.reimb_type_id_fk = reimb_type_id_fk;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "reimb_id=" + reimb_id +
                ", reimb_amount=" + reimb_amount +
                ", reimb_submitted=" + reimb_submitted +
                ", reimb_resolved=" + reimb_resolved +
                ", reimb_description='" + reimb_description + '\'' +
                ", reimb_receipt=" + reimb_receipt +
                ", reimb_author_fk=" + reimb_author_fk +
                ", reimb_resolver_fk=" + reimb_resolver_fk +
                ", reimb_status_id_fk=" + reimb_status_id_fk +
                ", reimb_type_id_fk=" + reimb_type_id_fk +
                '}';
    }
}


