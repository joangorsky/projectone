package services;

import models.Reimbursement;

import java.util.List;

public interface FMService {

    List<Reimbursement> viewAllReimbursements();
    void approveOrDenyReq(Integer reimbid, Integer reimb_status_id_fk);
    List<Reimbursement>viewAllPendingReq(Integer reimb_status_id_fk);
}
