package services;

import dao.FMDao;
import dao.FMDaoImpl;
import models.Reimbursement;

import java.util.List;

public class FMServiceImpl implements FMService {

    FMDao fmDao;

    public FMServiceImpl(){
        fmDao = FMDaoImpl.getInstance();
    }


    @Override
    public List<Reimbursement> viewAllReimbursements() {
        return fmDao.viewAllReimbursements();
    }

    @Override
    public void approveOrDenyReq(Integer reimbid, Integer reimb_status_id_fk) {
            fmDao.approveOrDenyReq(reimbid, reimb_status_id_fk);
    }

    @Override
    public List<Reimbursement> viewAllPendingReq(Integer reimb_status_id_fk) {
        return fmDao.viewAllReimbursements();
    }
}
