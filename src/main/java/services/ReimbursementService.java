package services;

import models.Reimbursement;

import java.util.List;

public interface ReimbursementService {

    List<Reimbursement> getAllPastTickets(Integer userid); // to get all past tickets
    void addReimbReq(Reimbursement reimbursement);// adding a new reimb request
    List<Reimbursement> viewStatus(Integer userid); //Viewing status of a ticket
}
