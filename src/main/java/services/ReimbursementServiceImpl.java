package services;

import dao.ReimbursementDao;
import dao.ReimbursementDaoImpl;
import models.Reimbursement;

import java.util.List;

public class ReimbursementServiceImpl implements ReimbursementService{
            ReimbursementDao reimbursementDao;

            public ReimbursementServiceImpl(){
                reimbursementDao = ReimbursementDaoImpl.getInstance();
            }

    @Override
    public List<Reimbursement> getAllPastTickets(Integer userid) {
        return reimbursementDao.getAllPastTickets(userid);
    }

    @Override
    public void addReimbReq(Reimbursement reimbursement) {

      //  System.out.println("in service - addreimbreq");

                reimbursementDao.addReimbReq(reimbursement);
    }

    @Override
    public List<Reimbursement> viewStatus(Integer userid) {
        return reimbursementDao.viewStatus(userid);
    }
}
