package services;

import models.User;

public interface UserService {

    User register(User user);
    User login(User user);
}
