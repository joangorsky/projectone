package services;

import dao.UserDao;
import dao.UserDaoImpl;
import models.User;

public class UserServiceImpl implements UserService{

    UserDao userDao;

    public UserServiceImpl(){
        userDao = UserDaoImpl.getInstance();
    }


    @Override
    public User register(User user) {
        //check if username exists in the system
        User tempUser = userDao.getOneUser(user.getErs_username());
        if(tempUser != null)
            return null;

        userDao.insertUser(user);

        return userDao.getOneUser(user.getErs_username());
    }

    @Override
    public User login(User user) {

       // System.out.println("in Userservicelogin");
        //check username exists in system
        User tempUser = userDao.getOneUser(user.getErs_username());
        if(tempUser == null)

            return null;

        //check if password is incorrect
        if(!tempUser.getErs_password().equals(user.getErs_password()))

            return null;

        return tempUser;
    }

}

