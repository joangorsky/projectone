//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const userid = urlParams.get("userid")

//function that gets called when window loads


window.onload = async function(){
    
     //check session
 /* const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionUserData = await sessionRes.json()

    console.log(sessionUserData)
    if(sessionUserData.data){
       if(sessionUserData.data.userid != userid){
           window.location = `${domain}/`
       } 
    }else{
        window.location = `${domain}/`
    }*/

}

    let addNewReimbReqForm = document.getElementById("addnewreq-form")
    
    addNewReimbReqForm.onsubmit = async function(e){

    e.preventDefault();


    let amount = document.getElementById("amount").value;
    let description = document.getElementById("description").value;
    let userid = document.getElementById("userid").value;
    let typeid = document.getElementById("type-id").value;
    console.log(amount,description,userid, typeid)

    
    let addnewreqresponse = await fetch(`${domain}/api/emp/addreimbreq`,{
        method: "POST",
        body: JSON.stringify({
            reimb_amount: amount,
            reimb_description: description,
            reimb_author_fk: userid,
            reimb_type_id_fk: typeid
            })
    })

    let responseData = await addnewreqresponse.json();
    console.log(responseData)
    
   if(responseData.success){
        window.location = `${domain}/empdashboard?userid=${userid}`
    }
    
}



//end session and redirect to empdashboard when button is clicked
let empDashBtn = document.getElementById("empdash-btn")
empDashBtn.onclick = async function(){
    window.location = `${domain}/empdashboard?userid=${userid}`
}
