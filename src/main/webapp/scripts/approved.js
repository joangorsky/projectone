//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const userid = urlParams.get("userid")


window.onload = async function(){

    approved();
}

async function approved(){

    const approvedRes = await fetch(`${domain}/api/fm/viewallpendingreq?reimb_status_id_fk=2`)

    const approvedData = await approvedRes.json()
            console.log(approvedData)

            let approvedContElem = document.getElementById("approved-container")
            approvedContElem.innerHTML = ``;

            approvedData.data.forEach(list =>{

            approvedContElem.innerHTML += `
            <div class = "emp-past-card">
                <div>Reimbursement Id: ${list.reimb_id}</div>
                <div>Reimbursement Amount: $${list.reimb_amount}</div>
                <div>Date Submitted: ${list.reimb_submitted}</div>
                <div>Date Resolved: ${list.reimb_resolved}</div>
                <div>Description: ${list.reimb_description}</div>
                <div>Receipt: ${list.reimb_receipt}</div>
                <div id = "status-id">Status: Approved</div>
                <div>Type: ${list.reimb_type_id_fk} </div>
             
              
             </div>   

            `
        })


}

