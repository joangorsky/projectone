//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const userid = urlParams.get("userid")


window.onload = async function(){

    denied();
}

async function denied(){

    const deniedRes = await fetch(`${domain}/api/fm/viewallpendingreq?reimb_status_id_fk=3`)

    const deniedData = await deniedRes.json()
            console.log(deniedData)

            let deniedContElem = document.getElementById("denied-container")
            deniedContElem.innerHTML = ``;

            deniedData.data.forEach(list =>{

            deniedContElem.innerHTML += `
            <div class = "fm-pend-card">

            <div class = "create-flex">
                
                <div>Reimbursement Id: ${list.reimb_id}</div>
                <div>Reimbursement Amount: $${list.reimb_amount}</div>
                <div>Date Submitted: ${list.reimb_submitted}</div>
                <div>Date Resolved: ${list.reimb_resolved}</div>
                <div>Description: ${list.reimb_description}</div>
                <div>Receipt: ${list.reimb_receipt}</div>
                <div id = "status-id">Status: Denied</div>
                <div>Type: ${list.reimb_type_id_fk} </div>
                
                </div>
             </div>   

            `
        })


}
