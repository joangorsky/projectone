//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const userid = urlParams.get("userid")

//function that gets called when window loads


window.onload = async function(){

    viewStatus();
    //check session
    const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionUserData = await sessionRes.json()

    console.log(sessionUserData)
    if(sessionUserData.data){
       if(sessionUserData.data.userid != userid){
           window.location = `${domain}/`
       } 
    }else{
        window.location = `${domain}/`
    }
    
        
}

async function viewStatus(){

    const viewStatusRes = await fetch (`${domain}/api/emp/viewstatus?userid=${userid}`)
    const viewStatusData = await viewStatusRes.json()
            console.log(viewStatusData)

            let pendContElem = document.getElementById("pending-container")
            pendContElem.innerHTML = ``;

            viewStatusData.data.forEach(list =>{

            pendContElem.innerHTML += `
            <div class = "emp-pend-card">
                <div id = "name">Pending Request</div>
                <div>Reimbursement Id: ${list.reimb_id}</div>
                <div>Reimbursement Amount: $${list.reimb_amount}</div>
                <div>Date Submitted: ${list.reimb_submitted}</div>
                <div>Date Resolved: ${list.reimb_resolved}</div>
                <div>Description: ${list.reimb_description}</div>
                <div>Receipt: ${list.reimb_receipt}</div>
                <div>Author ID: ${list.reimb_author_fk}</div>
                <div>Manager ID: ${list.reimb_resolver_fk}</div>
                <div id = "status-id">Status: Pending</div>
                <div>Type: ${list.reimb_type_id_fk} </div>
             </div>   

            `
        })

}


let addNewReimbReqBtn = document.getElementById("addnewreimbreq-btn")

addNewReimbReqBtn.onclick = function(){
    window.location = `${domain}/addnewreq?userid=${userid}`
} 


let getPastReq = document.getElementById("getallpasttickets-btn")

getPastReq.onclick = function(){
    window.location = `${domain}/emppastreq?userid=${userid}` 
} 



//end session and redirect to login when logout button is clicked
let logoutBtn = document.getElementById("logout-btn")
logoutBtn.onclick = async function(){
    let logoutRes = await fetch(`${domain}/api/logout`)

    let logoutResData = await logoutRes.json();

    if(logoutResData.success)
        window.location = `${domain}/`
}



