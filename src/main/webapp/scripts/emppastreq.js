//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const userid = urlParams.get("userid")

//function that gets called when window loads


window.onload = async function(){

    getAllPastTickets();
}

async function getAllPastTickets()

{
    const getAllPastTicketsRes = await fetch (`${domain}/api/emp/getallpasttickets?userid=${userid}`)

    const responseData = await getAllPastTicketsRes.json()
    console.log(responseData)


    let pastContElem = document.getElementById("past-container")
    pastContElem.innerHTML = ``;

    responseData.data.forEach(list =>{

    pastContElem.innerHTML += `
    <div class = "emp-past-card">
        <div id = "name">Past Request</div>
        <div>Reimbursement Id: ${list.reimb_id}</div>
        <div>Reimbursement Amount: $${list.reimb_amount}</div>
        <div>Date Submitted: ${list.reimb_submitted}</div>
        <div>Date Resolved: ${list.reimb_resolved}</div>
        <div>Description: ${list.reimb_description}</div>
        <div id = "receipt">Receipt: Receipt</div>
        <div> Status: ${list.reimb_status_id_fk}</div>
        <div>Type: ${list.reimb_type_id_fk} </div>
     </div>   

    `
})

}


//end session and redirect to login when logout button is clicked
let logoutBtn = document.getElementById("logout-btn")
logoutBtn.onclick = async function(){
    let logoutRes = await fetch(`${domain}/api/logout`)

    let logoutResData = await logoutRes.json();

    if(logoutResData.success)
        window.location = `${domain}/`
}

