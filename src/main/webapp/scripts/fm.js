//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const reimb_status_id_fk = urlParams.get("reimb_status_id_fk")
const userid = urlParams.get("userid")


//function that gets called when the window loads
window.onload= async function(){

    viewAllPendingRequests();

    /* //check session
     const sessionRes = await fetch(`${domain}/api/check-session`)
     const sessionUserData = await sessionRes.json()
 
     console.log(sessionUserData)
     if(sessionUserData.data){
        if(sessionUserData.data.userid != userid){
            window.location = `${domain}/`
        } 
     }else{
         window.location = `${domain}/`
     }*/

   
}


async function viewAllPendingRequests() {

    const viewAllPendingRequestsRes = await fetch(`${domain}/api/fm/viewallpendingreq?reimb_status_id_fk=1`)

    const responseData = await viewAllPendingRequestsRes.json()
                console.log(responseData)

                let pendContElem = document.getElementById("pending-container")
            pendContElem.innerHTML = ``;

            responseData.data.forEach(list =>{

            pendContElem.innerHTML += `
            <div class = "fm-pend-card">

            <div class = "create-flex">
                <div id = "name">Pending Request</div>
                <div>Reimbursement Id: ${list.reimb_id}</div>
                <div>Reimbursement Amount: $${list.reimb_amount}</div>
                <div>Date Submitted: ${list.reimb_submitted}</div>
                <div>Date Resolved: ${list.reimb_resolved}</div>
                <div>Description: ${list.reimb_description}</div>
                <div id = "receipt">Receipt: Receipt</div>
                 <div id = "status-id">Status: Pending</div>
                <div>Type: ${list.reimb_type_id_fk} </div>
                <br>
                <br>
                <div>
                    <button class="btn btn-success" onclick = "approveButtonFunction(${list.reimb_id})" id="approve-btn">Approve</button>
                    <button class="btn btn-danger" onclick = "denyButtonFunction(${list.reimb_id})" id="deny-btn">Deny</button>
                </div>
                </div>
             </div>   

            `
        })


}

        
let approveButton = document.getElementById("approve-btn")
async function approveButtonFunction(reimb_id){

  
   let approveRequestRes = await fetch(`${domain}/api/fm/approveordenyreq?reimbid=${reimb_id}&reimb_status_id_fk=2`,
   {method: "PUT"})
   

    let approveRequestData = await approveRequestRes.json()
       console.log(approveRequestData)

   }

    let denyButton = document.getElementById("deny-btn")
    async function denyButtonFunction(reimb_id){
    const denyRequestRes = await fetch(`${domain}/api/fm/approveordenyreq?reimbid=${reimb_id}&reimb_status_id_fk=3`,
    {method: "PUT"})

    const denyRequestData = await denyRequestRes.json()
       console.log(denyRequestData)

  
}
     


let approvedBtn = document.getElementById("approved-btn")

approvedBtn.onclick = function(){
    window.location = `${domain}/approved?userid=${userid}`
} 


let deniedBtn = document.getElementById("denied-btn")

deniedBtn.onclick = function(){
    window.location = `${domain}/denied?userid=${userid}` 
} 


//end session and redirect to login when logout button is clicked
let logoutBtn = document.getElementById("logout-btn")
logoutBtn.onclick = async function(){
    let logoutRes = await fetch(`${domain}/api/logout`)

    let logoutResData = await logoutRes.json();

    if(logoutResData.success)
        window.location = `${domain}/`
}





/*
  let reimbid = reimb_id;
    let reimbstatus = 2;

    let approveRequestRes = await fetch(`${domain}/api/fm/approveordenyreq`,{
        method: "PUT",
        body: JSON.stringify({
           reimb_id: reimbid,
            reimb_status_id_fk: reimbstatus,
         })
    })*/