let loginForm = document.getElementById("login-form");

loginForm.onsubmit = async function(e){
    e.preventDefault();
    

    //get values from the input field
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;

    console.log(username,password)

    //how do we send values to the backend?
    let response = await fetch(`${domain}/api/login`,{
        method: "POST",
        body: JSON.stringify({
            ers_username: username,
            ers_password: password
        })
    })

    let responseData = await response.json();
    console.log(responseData)

     if(responseData.success){
        
        if(responseData.data.user_role_id_fk == 1)
            {
                console.log("in employee")
                console.log(responseData.data.ers_users_id)

    window.location = `${domain}/empdashboard?userid=${responseData.data.ers_users_id}`
            }
                
          if(responseData.data.user_role_id_fk == 2)
           {
     window.location = `${domain}/fmdashboard?userid=${responseData.data.ers_users_id}`
          }
}
else{
    let messageElem = document.getElementById("login-message")
    messageElem.style = "background-color: white;"
    messageElem.innerText = responseData.message
} 


}
